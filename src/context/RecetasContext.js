import React, {createContext, useEffect, useMemo, useState} from "react";
import axios from "axios";

export const RecetasContext = createContext();

const RecetasProvider = (props) => {

    const [recetas, guardarRecetas] = useState([]);
    const [busqueda, buscarRecetas] = useState({
        nombre: '',
        categoria: ''
    });

    const [consultar, guardarConultar] = useState(false);

    const {nombre, categoria} = busqueda;

    useEffect(() => {
        if (consultar) {
            const obtenerRecetas = async () => {
                const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${nombre}&c=${categoria}`;
                const resultado = await axios.get(url);
                guardarRecetas(resultado.data.drinks);
            }
            obtenerRecetas();
            guardarConultar(false)
        }
    },[busqueda,categoria,consultar,nombre]);

    return (
        <RecetasContext.Provider
            value={
                useMemo(() => ({
                    recetas,
                    buscarRecetas,
                    guardarConultar
                }), [recetas, buscarRecetas, guardarConultar])
            }
        >
            {props.children}
        </RecetasContext.Provider>
    )
}

export default RecetasProvider;