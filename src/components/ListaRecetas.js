import React, { useContext } from 'react';
import {RecetasContext} from "../context/RecetasContext";
import Receta from "./Receta";
import useRecetaModal from "../hooks/useRecetaModal";

function ListaRecetas(props) {
    // Extraer Recetas
    const {recetas} = useContext(RecetasContext);
    const [CustomModal,handleOpen, setRecetaId] = useRecetaModal();

    return (
        <div className="row mt-5">
            {recetas.map(receta => (
                <Receta
                    key={receta.idDrink}
                    receta={receta}
                />
            ))}
        </div>
    );
}

export default ListaRecetas;