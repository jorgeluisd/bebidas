import React from 'react';

function Header(props) {
    return (
        <header className="bg-alert">
            <h1>Buscar Recetas de Bebidas</h1>
        </header>
    );
}

export default Header;