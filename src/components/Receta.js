import React, { useContext, useState } from 'react';
import useRecetaModal from "../hooks/useRecetaModal";

function Receta({receta}) {

    const [CustomModal,handleOpen, setRecetaId, recetaId] = useRecetaModal();

    return (
        <div className="col-md-4 mb-3">
            <div className="card">
                <h2 className="card-header">{receta.strDrink}</h2>

                <img src={receta.strDrinkThumb} alt={`Imagen de ${receta.strDrink}`} className="card-img-top"/>

                <div className="card-body">
                    <button
                        type="button"
                        className="btn btn-block btn-primary"
                        onClick={() => {
                            setRecetaId(receta.idDrink);
                            handleOpen();
                        }}
                    >
                        Ver Receta
                    </button>
                    {recetaId ? <CustomModal /> : null}

                </div>
            </div>
        </div>
    );
}

export default Receta;