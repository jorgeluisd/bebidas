import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Modal} from "@material-ui/core";
import axios from "axios";

function useRecetaModal() {

    const [open, setOpen] = useState(false);
    const useStyles = makeStyles(theme => ({
        paper: {
            position: 'absolute',
            width: 400,
            backgroundColor: theme.palette.background.paper,
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
            overflow: 'scroll',
            height: '100%',
            maxHeight: 500,
            display: 'block'
        },
        header: {
            padding: '12px 0',
            borderBottom: '1px solid darkgrey'
        },
        content: {
            padding: "12px 0",
            overflow: 'scroll'
        }
    }));
    const [modalStyle] = useState(getModalStyle);
    const [recetaId, setRecetaId] = useState();
    const [informacion, guardarReceta] = useState({});

    // Al tener la receta, llamar a la API
    useEffect(() => {
        const obtenerReceta = async () => {
            if (!recetaId) return;

            const url = `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${recetaId}`;
            const respuesta = await axios.get(url);
            guardarReceta(respuesta.data.drinks[0]);
        }
        obtenerReceta();
    }, [recetaId]);

    function getModalStyle() {
        const top = 50 ;
        const left = 50;

        return {
            top: `${top}%`,
            left: `${left}%`,
            transform: `translate(-${top}%, -${left}%)`,
        };
    }

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    // Muestra y formatea los ingredientes
    const mostrarIngredientes = informacion => {
        let ingredientes = [];
        for (let i = 1; i < 16; i++) {
            if (informacion[`strIngredient${i}`]) {
                ingredientes.push(
                    <li>{informacion[`strIngredient${i}`]} {informacion[`strMeasure${i}`]}</li>
                )
            }
        }
        return ingredientes;
    }

    const CustomModal = () => {
        const classes = useStyles();

        return (
            <Modal
                open={open}
                onClose={() => {
                    setRecetaId(null);
                    guardarReceta({})
                    handleClose();
                }}
            >
                <div style={modalStyle} className={classes.paper}>
                    <h2>{informacion.strDrink}</h2>

                    <h3 className="mt-4">Instrucciones</h3>
                    <p>
                        {informacion.strInstructions}
                    </p>

                    <img src={informacion.strDrinkThumb} alt="" className="img-fluid my-4"/>

                    <h3>Ingredientes y Cantidades</h3>
                    <ul>
                        { mostrarIngredientes(informacion) }
                    </ul>
                </div>
            </Modal>
        );
    }

    return [CustomModal,handleOpen,setRecetaId,recetaId];
}

export default useRecetaModal;